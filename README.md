## Overview

This is a wrapper for using Mozilla's PDF.js in a React component. https://github.com/mozilla/pdf.js/

There are a few other examples out there ( this one was particularly useful: https://github.com/seges/react-pdfjs-example )

This builds on those by adding a few abilities:  

1.  The text layer - for selectable/searchable text  
2.  The ability to scroll to a specific page (or to determine which page the user has already scrolled to)  

---

## Usage

```
import PdfJsFrame from 'pdfjs-react-frame';

<PdfJsFrame
	style={/* optional style object */}
	url={/* URL of a document to fetch - this is required if data is null */}
	data={/* A document to display as an arrayBuffer - this is required if URL is null */}
	pageNumber={/* The page to scroll to (default: 1) */}
	onPageChange={/* optional function to handle page changes, (pageNumber) => { } */}
	workerPath=/* optional (default: Mozilla GitHub) path to serve the worker file locally '/pdf.worker.min.js' */
	loadingElement={/* optional, e.g. <CircularProgress/> */}
/>
```

https://developer.mozilla.org/en-US/docs/Web/API/Body/arrayBuffer

---

## Serve Worker.js Locally

This is a bit of a hold-over from the fact that the library isn't really designed for React.
I couldn't think of a better way to do this without a fairly big rewrite to PDF.js...

In order to use this, you'll also need to ensure that the file is available and served from your application, e.g. by adding this to your package.json
```
"postinstall": "echo D|xcopy node_modules\\pdfjs-dist\\build\\pdf.worker.min.js public /sy"
```
