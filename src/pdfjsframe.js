﻿import React from 'react';
import { Component } from 'react';
import uuid from 'uuid';

import * as PdfJs from 'pdfjs-dist';
import * as PdfJsText from './lib/pdfjs/text_layer_builder';
import './lib/pdfjs/text_layer_builder.css';

const classes = Object.freeze({
  pdfFrame: {
    width: 600,
    height: 800,
    overflowY: 'scroll',
    position: 'relative'
  }
});

class PdfJsFrame extends Component {
  constructor(props) {
    super(props);

    PdfJs.GlobalWorkerOptions.workerSrc = props.workerPath || '//mozilla.github.io/pdf.js/build/pdf.worker.js';

    this.reload = this.reload.bind(this);
    this.attachPdfImage = this.attachPdfImage.bind(this);
    this.attachPdfText = this.attachPdfText.bind(this);
    this.setCanvasContainerRef = this.setCanvasContainerRef.bind(this);
    this.renderPage = this.renderPage.bind(this);
    this.scrollToPage = this.scrollToPage.bind(this);
    this.handleScroll = this.handleScroll.bind(this);

    this.id = 'pdf-container-' + uuid.v4();
    this.pageNumber = this.props.pageNumber || 1;

    this.state = { pdfPages: [], loading: false, maxWidth: null };
  }

  async componentDidMount() {
    this.reload();
    document.getElementById(this.id).addEventListener('scroll', this.handleScroll);
  }

  async componentWillUnmount() {
    document.getElementById(this.id).removeEventListener('scroll', this.handleScroll);
  }

  async reload() {
    let { data, url, style } = this.props;
    if (data || url) {
      await this.setState({ pdfPages: [], loading: true });

      let pdf = await PdfJs.getDocument(data ? { data } : url).promise;

      var viewportHeightTotal = 0;
      var maxWidth = 0;
      let pdfPages = [];
	  let scale = style && style.width && !isNaN(style.width) ? ((style.width - 27) / 600) : ((600 - 27) / 600);

      for (var i = 1; i <= pdf.numPages; i++) {
        let page = await pdf.getPage(i);

        let viewport = page.getViewport({ scale });
        let text = await page.getTextContent();

        pdfPages.push({ page, viewport, text, index: i, top: viewportHeightTotal });

        viewportHeightTotal += viewport.height;
        if (viewport.width > maxWidth) maxWidth = viewport.width;
      }

      await this.setState({ pdfPages, loading: false, maxWidth });
    }

    await this.scrollToPage(this.pageNumber);
  }

  async componentDidUpdate(p, n) {
    if (this.props.url !== p.url || this.props.data !== p.data) {
      this.reload();
    }
    else if (this.props.pageNumber !== this.pageNumber) {
      this.scrollToPage(this.props.pageNumber);
      this.pageNumber = this.props.pageNumber;
    }
  }

  async scrollToPage(num) {
    let el = document.getElementById(this.id);
    if (el && this.state.pdfPages[num - 1]) {
      el.scrollTop = this.state.pdfPages[num - 1].top;
    }
    else {
      el.scrollTop = 0;
    }
  }

  async handleScroll() {
    let page = this.state.pdfPages.find(p => p.top >= document.getElementById(this.id).scrollTop);
    let pageNum = page ? page.index : this.state.pdfPages.length;

    if (pageNum !== this.pageNumber) {
      this.pageNumber = pageNum;
      if (this.props.onPageChange)
        await this.props.onPageChange(pageNum);
    }
  }

  setCanvasContainerRef(c) {
    this.canvasContainer = c;
    this.canvasContainerDims = c && c.getBoundingClientRect();
  }

  async attachPdfImage(canvas, page) {
    if (canvas && !page.imgAttached) {
      page.imgAttached = true;

      let canvasContext = canvas.getContext('2d');
      await page.page.render({
        canvasContext,
        viewport: page.viewport
      }).promise;
    }
  }

  async attachPdfText(div, page) {
    if (div && !page.txtAttached) {
      page.txtAttached = true;

      var textLayer = new PdfJsText.TextLayerBuilder({
        textLayerDiv: div,
        pageIndex: page.index,
        viewport: page.viewport
      });

      textLayer.setTextContent(page.text);
      textLayer.render();
    }
  }

  renderPage(page) {
    var dims = this.canvasContainerDims || { width: 600, height: 800 };

    return <React.Fragment key={"pdfjs-page-" + page.index}>
      <canvas
        ref={async (me) => await this.attachPdfImage(me, page)}
        width={dims.width - 27}
        height={page.viewport.height}
      />
      {this.canvasContainer ? <div className='textLayer'
        ref={async (me) => await this.attachPdfText(me, page)}
        style={{ width: dims.width - 27, height: dims.height, left: 0, top: page.top }}
      ></div> : null}
    </React.Fragment>;
  }

  render() {
    let { style } = this.props;
    let divStyle = Object.assign({},
      classes.pdfFrame,
      this.state.maxWidth ? { width: this.state.maxWidth + 27 } : {},
      style
    ); //defaults <- from docs <- inputs
      
    return <div style={divStyle} ref={this.setCanvasContainerRef} id={this.id}>
      {this.state.loading && <div style={{ width: '100%', textAlign: 'center', marginTop: isNaN(divStyle.height) ? 25 : divStyle.height / 25 }}>{this.props.loadingElement || 'loading ...'}</div>}
      {this.state.pdfPages.map(this.renderPage)}
    </div>;
  }
}

export default PdfJsFrame;
